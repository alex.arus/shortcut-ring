# Shortcut-Ring

Shortcut-Ring to rule them all. Intuitive and easy to remember keyboard shortcuts scheme to rule your computer and increase productivity.

# Legend

Super - to control an operating system and widow manager
Ctrl - to command
Alt - an alternative
Shift - to emphasize or select
Tab - to switch

## Context

Operating system (OS)
Window manager
Application with tabs/panels
Text Editor

- Alt - to control caret
- Alt+Shift - to control caret and selection
- Alt+Space - tab

Form

# Setup

- Find script for your application in script folder. For example for Gnome DE 'setupShortcutRing-Gnome.js'.
- Make script executable

```
chmod +x setupShortcutRing-Gnome.js
```

- Execute script

```
./setupShortcutRing-Gnome.js
```

## Reset

- Find script for your application in script folder. For example for Gnome DE 'resetShortcutRing-Gnome.js'.
- Make script executable

```
chmod +x resetShortcutRing-Gnome.js
```

- Execute script

```
./resetShortcutRing-Gnome.js
```

## IntelliJ IDEA and other (doesn't work with "IDE Settings Sync" pluging)

Copy file ide/Shortcut-Ring.xml to IDE folder /config/keymaps/. Home folder should look like ~/.IdeaIC2019.1 (for IntelliJ CE, full path in that case would be ~/.IdeaIC2019.1/config/keymaps/).

```
cp ide/Shortcut-Ring.xml ~/.IdeaIC2019.1/config/keymaps/
```


# Shortcuts

## Operating system (OS)
## Window manager
## Application with tabs/panels
## Text Editor
