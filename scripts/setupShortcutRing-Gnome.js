#!/usr/bin/env node

const execSync = require('child_process').execSync

function setup(key, shortcut, path) {
    const cmd = `gsettings set ${path} ${key} "${shortcut}"`;
    console.log(cmd);
    execSync(cmd, {stdio: 'inherit'})
}

/**
 * @typedef {Object} Keybinding
 * @property {string} key
 * @property {string[]} value
 *
 * @typedef {Object} KeybindingsGroup
 * @property {string} path
 * @property {Keybinding[]} keybindings
 *
 */

/**
 * @type {KeybindingsGroup[]} - shortcuts
 */
const shortcuts = [
    {
        path: 'org.gnome.desktop.wm.keybindings',
        keybindings: [
            {
                key: 'close',
                value: `['<Super>Escape','<Alt>F4']`,
            },
            {
                key: 'maximize',
                value: `['<Super>i','<Super>Up']`,
            },
            {
                key: 'unmaximize',
                value: `['<Super>k', '<Super>Down']`,
            },
            {
                key: 'activate-window-menu',
                value: `['<Super>space']`,
            },
            {
                key: 'move-to-monitor-left',
                value: `['<Super><Shift>j','<Super><Shift>Left']`,
            },
            {
                key: 'move-to-monitor-right',
                value: `['<Super><Shift>l','<Super><Shift>Right']`,
            },
            {
                key: 'move-to-workspace-down',
                value: `['<Shift><Super>o','<Super><Shift>Page_Down']`,
            },
            {
                key: 'move-to-workspace-up',
                value: `['<Super><Shift>u','<Super><Shift>Page_Up']`,
            },
            {
                key: 'switch-to-workspace-down',
                value: `['<Super>o','<Super>Page_Down']`,
            },
            {
                key: 'switch-to-workspace-up',
                value: `['<Super>u','<Super>Page_Up']`,
            },
            {
                key: 'switch-to-workspace-1',
                value: `['<Super>q']`,
            },
            {
                key: 'switch-to-workspace-2',
                value: `['<Super>w']`,
            },
            {
                key: 'switch-to-workspace-3',
                value: `['<Super>e']`,
            },
            {
                key: 'switch-to-workspace-4',
                value: `['<Super>r']`,
            },
        ]
    }, {
        path: 'org.gnome.mutter.keybindings',
        keybindings: [
            {
                key: 'toggle-tiled-left',
                value: `['<Super>j']`,
            },
            {
                key: 'toggle-tiled-right',
                value: `['<Super>l']`,
            },
        ],
    }
];

shortcuts.forEach(({path, keybindings}) =>
    keybindings.forEach(({key, value}) => setup(key, value, path))
);
