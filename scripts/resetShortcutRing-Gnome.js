#!/usr/bin/env node

const execSync = require('child_process').execSync

function reset(path, key) {
    const cmd = `gsettings reset ${path} ${key}`;
    console.log(cmd);
    execSync(cmd, {stdio: 'inherit'})
}

/**
 * @typedef {Object} KeybindingsGroup
 * @property {string} path
 * @property {string[]} keybindings
 *
 */

/**
 * @type {KeybindingsGroup[]} - actions
 */
const actions = [
    {
        path: 'org.gnome.desktop.wm.keybindings',
        keybindings: [
            'close',
            'maximize',
            'unmaximize',
            'activate-window-menu',
            'move-to-monitor-left',
            'move-to-monitor-right',
            'move-to-workspace-down',
            'move-to-workspace-up',
            'switch-to-workspace-down',
            'switch-to-workspace-up',
            'switch-to-workspace-1',
            'switch-to-workspace-2',
            'switch-to-workspace-3',
            'switch-to-workspace-4',
        ],
    }, {
        path: 'org.gnome.mutter.keybindings',
        keybindings: [
            'toggle-tiled-left',
            'toggle-tiled-right',
        ],
    },
];

actions.forEach(({path, keybindings}) => {
    keybindings.forEach(action => reset(path, action))
});
